package com.biom4st3r.incense;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;

public class IncenseMod implements ModInitializer, ClientModInitializer
{
    //BrewingRecipeRegistry
    //BrewingStandBlock
    public static IncenseConfig config;

    @Override
    public void onInitializeClient() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onInitialize() {
        File file = new File(FabricLoader.getInstance().getConfigDirectory().getPath(), "moenchantconfig.json");
		try {
			//util.logger("loading config!", true);
			FileReader fr = new FileReader(file);
			config = new Gson().fromJson(fr, IncenseConfig.class);
			FileWriter fw = new FileWriter(file);
			fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
			fw.close();
		} catch (IOException e) {
			//util.logger("failed loading! Creating initial config!", true);
			config = new IncenseConfig();
			try {
				FileWriter fw = new FileWriter(file);
				fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
				fw.close();
			} catch (IOException e1) {
				//util.logger("failed config!", true);
				e1.printStackTrace();
			}
		}


    }

}