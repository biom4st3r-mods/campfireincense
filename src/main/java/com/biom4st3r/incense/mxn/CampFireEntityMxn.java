package com.biom4st3r.incense.mxn;

import java.util.Map;
import java.util.Random;

import com.biom4st3r.incense.IncenseMod;
import com.biom4st3r.incense.interfaces.Incense;
import com.google.common.collect.Maps;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.block.CampfireBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.CampfireBlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.potion.Potion;
import net.minecraft.util.Clearable;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.Box;

@Mixin(CampfireBlockEntity.class)
public abstract class CampFireEntityMxn extends BlockEntity implements Clearable, Tickable, Incense {

    Map<StatusEffect,Integer> effects;// = Maps.newHashMap();

    public CampFireEntityMxn(BlockEntityType<?> blockEntityType_1) {
        super(blockEntityType_1);
    }

    @Inject(at = @At("RETURN"),method = "<init>*")
    public void onConst(CallbackInfo ci)
    {
        effects = Maps.newHashMap();
    }

    @Override
    public boolean addPotion(Potion p) {
        if(effects.size() >= IncenseMod.config.maxPotionEffects)
        {            
            //System.out.println(effects.size());
            return false;
        }
        else if(p.getEffects().size() == 1 && p.getEffects().get(0).getEffectType().isInstant())
        {
            return false;
        }
        else
        {
            //System.out.println(p.getEffects().size());
            p.getEffects().stream().forEach((se)->
            {
                if(!se.getEffectType().isInstant())
                    effects.put(se.getEffectType(),IncenseMod.config.durationOfEffectsInTicks);
                //System.out.println(effects.size());
            });
            return true;
        }
    }

    int counter = 0;
    @Inject(at = @At("HEAD"),method="tick")
    public void tick(CallbackInfo ci)
    {
        boolean isLit = this.getCachedState().get(CampfireBlock.LIT);
        if(isLit)
        {
            for(StatusEffect se : effects.keySet())
            {
                int remaining = effects.get(se);
                if(remaining <= 1)
                {
                    effects.remove(se);
                    break;
                }
                effects.replace(se, remaining-1);
            }
            if(counter >= 20)
            {
                tick20();
            }
            else
            {
                ++counter;
            }
        }
    }

    

    public void tick20()
    {
        counter = 0;
        if(this.world.isClient && effects.size() > 0)
        {
            Random rand = this.world.getRandom();
            for(int i = 0; i < 20; i++)
            {
                StatusEffect se = (StatusEffect)effects.keySet().toArray()[this.world.random.nextInt(effects.size())];
                int color = se.getColor();
                double r = (double)(color >> 16 & 255) / 255.0D;
                double g = (double)(color >> 8 & 255) / 255.0D;
                double b = (double)(color >> 0 & 255) / 255.0D;
                this.world.addParticle(ParticleTypes.ENTITY_EFFECT, 
                    this.pos.getX() + (rand.nextDouble()), 
                    this.pos.getY() + (1f*i)/20,//rand.nextDouble(), 
                    this.pos.getZ() + (rand.nextDouble()), r, g, b);
            }
        }
        int radius = IncenseMod.config.entitySearchRadius;
        this.world.getEntities(LivingEntity.class, 
            new Box(this.getPos().getX()-radius, this.getPos().getX()-1, this.getPos().getZ()-radius,
                this.getPos().getX()+radius, this.getPos().getY()+radius, this.getPos().getZ()+radius), 
            (e -> e instanceof LivingEntity)).stream().forEach((le)->
        {
            for(StatusEffect se : effects.keySet())
            {
                le.addPotionEffect(new StatusEffectInstance(se,20));
            }
        });
    }



}