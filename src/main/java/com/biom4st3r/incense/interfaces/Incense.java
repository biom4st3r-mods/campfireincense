package com.biom4st3r.incense.interfaces;

import net.minecraft.potion.Potion;

public interface Incense
{
    public boolean addPotion(Potion p);
}