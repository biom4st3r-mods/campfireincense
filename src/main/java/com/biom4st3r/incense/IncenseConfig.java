package com.biom4st3r.incense;

public class IncenseConfig
{

    public int maxPotionEffects;
    public int durationOfEffectsInTicks;
    public int entitySearchRadius;

    public IncenseConfig()
    {
        maxPotionEffects = 4;
        durationOfEffectsInTicks = 20 * 60 * 1; //20 ticks * 60 seconds
        entitySearchRadius = 7;
        
    }

}